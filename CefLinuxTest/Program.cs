﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.OffScreen;

namespace CefLinuxTest {
	class Program {
		static void Main(string[] args) {
			const string testUrl = "https://www.google.com/";

			Console.WriteLine("You may see Chromium debugging output, please wait...");
			Console.WriteLine();

			var settings = new CefSettings() {
				//By default CefSharp will use an in-memory cache, you need to specify a Cache Folder to persist data
				CachePath = Path.Combine(
					Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
					"CefSharpX\\Cache"
				)
			};

			//Perform dependency check to make sure all relevant resources are in our output directory.
			Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null);

			// Create the offscreen Chromium browser.
			var browser = new ChromiumWebBrowser(testUrl);

			EventHandler<LoadingStateChangedEventArgs>? handler = null;

			handler = (s, e) => {
				// Check to see if loading is complete - this event is called twice, one when loading starts
				// second time when it's finished
				if (e.IsLoading) {
					return;
				}

				// Remove the load event handler, because we only want one snapshot of the page.
				browser.LoadingStateChanged -= handler;
				Thread.Sleep(500);

				// Wait for the screenshot to be taken.
				var task = browser.CaptureScreenshotAsync();
				task.ContinueWith(x => {
					// File path to save our screenshot e.g. C:\Users\{username}\Desktop\CefSharp screenshot.png
					var screenshotPath = Path.Combine(
						Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
						"CefSharpScreenshot.png"
					);

					Console.WriteLine();
					Console.WriteLine("Screenshot ready. Saving to {0}", screenshotPath);

					var bitmapAsByteArray = x.Result;

					// Save the Bitmap to the path.
					File.WriteAllBytes(screenshotPath, bitmapAsByteArray);

					Console.WriteLine("Screenshot saved.  Launching your default image viewer...");
				}, TaskScheduler.Default);
			};

			// An event that is fired when the first page is finished loading.
			// This returns to us from another thread.
			browser.LoadingStateChanged += handler;

			// We have to wait for something, otherwise the process will exit too soon.
			Console.ReadKey();

			// Clean up Chromium objects. You need to call this in your application otherwise
			// you will get a crash when closing.
			//The ChromiumWebBrowser instance will be disposed
			Cef.Shutdown();
		}
	}
}